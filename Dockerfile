FROM ruby:2.7-alpine

RUN apk add --no-cache yarn tzdata libxml2-dev cmake curl-dev git make gcc libc-dev g++ mariadb-dev
